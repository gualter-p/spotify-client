import React from 'react'


const NoTokenError = () => (
    <div>
        <h1>Ops, you have to login first</h1>
    </div>
)

export default NoTokenError