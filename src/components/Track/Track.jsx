import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import {Row, Container} from 'react-bootstrap'
import {Play, Heart, HeartFill} from 'react-bootstrap-icons'
import {spotifyApi} from '../../auth/SpotifyAPI'

const Track = ({track, chooseTrack}) => {

    // State for liking track
    const [liked, setLiked] = useState(false)

    // To handle track choice
    const handleChoice = () => {
        chooseTrack(track)
    }

    // To çike a track
    const handleLike = () => {
        spotifyApi.addToMySavedTracks([track.trackID]).then(data => {
            setLiked(true)
        }, err => {
            console.log("Something went wrong", err)
        })
    }

    // To dislike a track
    const handleDislike = () => {
        spotifyApi.removeFromMySavedTracks([track.trackID]).then(data => {
            setLiked(false)
        }, err => {
            console.log("Something went wrong", err)
        })
    }

    return (
        <Container className="d-flex m-2 align-items-center">
            <img src={track.albumUrl} alt="" style={{ height: "65px", width: "65px"}}/>
            <div className="ml-3 right-align">
                <div className="align-items-center">        
                    {track.title} 
                    {liked ? <HeartFill size={15} className="col- ml-2" style={{cursor: "pointer"}}
                              onClick={handleDislike}/> 
                           : <Heart size={15} className="col- ml-2" style={{cursor: "pointer"}} 
                              onClick={handleLike}/>
                    }                                                        
                    <Row className="ml-auto align-items-center">
                        Play it <Play style={{cursor: "pointer"}} onClick={handleChoice} size={20}/> 
                    </Row>  
                </div>
                <Link to={`/artist/${track.artistID}`}>
                    <div className="text-muted">
                        {track.artist}
                    </div>
                </Link>       
            </div>
        </Container>
    )
}

export default Track