import React, {useState, useEffect} from 'react'
import { spotifyApi } from '../../auth/SpotifyAPI'
import {Container, Row} from 'react-bootstrap'
import {XCircle} from 'react-bootstrap-icons'

const SavedTracks = () => {

    // State for saved tracks
    const [savedTracks, setSavedTracks] = useState([])

    // State for track removal
    const [removedSwitch, setRemovedSwitch] = useState(false)

    // Remove track from saved tracks
    const handleRemoveFromSavedTracks = trackID => {
        spotifyApi.removeFromMySavedTracks([trackID])
        .then( data => {
            setRemovedSwitch(!removedSwitch);
        }, err => {
            console.log('Something went wrong!', err);
        });
    }    

    // Get saved tracks
    useEffect(() => {
        // Make the request
        spotifyApi.getMySavedTracks({limit: 3})
          .then(data => {
            const result = data.body.items.map( track => {
                return {
                    trackName: track.track.name,
                    trackID: track.track.id,
                    trackArtist: track.track.artists[0].name,
                    trackAlbum: track.track.album.name,
                    trackAlbumURL: track.track.album.images[0].url
                }
            })          
            setSavedTracks(result)
          }, err => {
            console.log('Something went wrong!', err);
          });
    }, [removedSwitch])

    return(
        <div className="d-flex flex-column align-items-center">
            {savedTracks.map(track => (   
                <Container key={track.trackID} className="flex-grow-1 my-2">
                    <Row className="align-items-center m-2">
                        <img className="col- mr-2" src={track.trackAlbumURL} 
                             alt="" style={{ height: "35px", width: "35px"}}/>                        
                        <div className="col- mr-2">
                            {track.trackName}
                        </div>
                        <div className="col- text-muted">
                            {track.trackArtist}
                        </div>
                        <div className="col- ml-2">
                            <XCircle style={{cursor: "pointer"}} 
                            onClick={() => handleRemoveFromSavedTracks(track.trackID)}/>
                        </div>
                    </Row>                                 
                </Container>                    
            ))}
        </div>
    )
}

export default SavedTracks