import React, {useState, useEffect} from 'react'
import { spotifyApi } from '../../auth/SpotifyAPI'
import {Container} from 'react-bootstrap'

import Playlist from './Playlist'

const UserPlaylists = ({token, userid}) => {

    // State for user's playlists
    const [userPlaylists, setUserPlaylists] = useState([])

    // Get user playlists
    useEffect(() => {
        // Make the request
        spotifyApi.getUserPlaylists(userid).then( res => {  
            const result = res.body.items.map(playlist => {
                return {
                    playlistName: playlist.name,
                    playlistDesc: playlist.description,
                    playlistTracks: playlist.tracks,
                    playlistID: playlist.id
                }
            })
            setUserPlaylists(result) 
        })
    }, [userid])

    return (
        <Container>
            {userPlaylists.map(playlist => (
                <ul key={playlist.playlistID}>
                    <Playlist token={token} playlist={playlist}></Playlist>
                </ul>
            ))}
        </Container>    
    )
}

export default UserPlaylists