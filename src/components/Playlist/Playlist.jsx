import React, {useState, useEffect} from 'react'
import { spotifyApi } from '../../auth/SpotifyAPI'
import {Container, Card, Row, Button} from 'react-bootstrap'
import {XCircle} from 'react-bootstrap-icons'

import AddTrackToPlaylistModal from  '../Modals/AddTrackToPlaylistModal'

const Playlist = ({token, playlist}) => {

    // State for playlist tracks
    const [playlistTracks, setPlaylistTracks] = useState([])

    // State for opening modal
    const [modalIsOpen, setModalIsOpen] = useState(false)

    // State for track removal
    const [trackRemoved, setTrackRemoved] = useState(false)

    // To remove track from playlist
    const handleRemoveTrackFromPlaylist = (playlistID, trackURI, trackPos) => {
        spotifyApi.removeTracksFromPlaylist(playlistID, [{uri: trackURI, positions: [trackPos]}])
        .then(data => {
            setTrackRemoved(!trackRemoved)
            console.log('Tracks removed from playlist!');
          }, err => {
            console.log('Something went wrong!', err);
          });        
    }

    // Get playlist tracks
    useEffect(() => {
        // Make the request
        fetch(`https://api.spotify.com/v1/playlists/${playlist.playlistID}/tracks`, {
            method: 'GET', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token // No token when logged out
            }
        }).then( res => res.json())
          .then( data => data.items.map((track, i) => {

            // Extract album image url
            const getAlbumURL = url => {
                return url ? url : ""
            }

            // Extract artist name
            const getArtistName = name => {
                return name ? name : ""
            }

            return {
                trackName: track.track.name,
                trackURI: track.track.uri,
                trackPos: i,
                trackAlbum: track.track.album.name,
                trackAlbumUrl: getAlbumURL(track.track.album.images[0].url),
                trackArtist: getArtistName(track.track.artists[0].name),
                trackPreview: track.track.preview_url
            }
        })).then( res => {
            console.log(res)
            setPlaylistTracks(res) 
        }) 
       
    }, [playlist, token, modalIsOpen, trackRemoved])   

    return (
        <Container fluid> 
            <AddTrackToPlaylistModal token={token} playlist={playlist}
                                     isOpen={modalIsOpen} setModalOpen={setModalIsOpen}/>
            <Card border="secondary">
            <Card.Header>
                <Row className="ml-1" >
                    <Card.Title>{playlist.playlistName}</Card.Title>
                    <div className="ml-auto px-4 pt-2">
                        <Button onClick={() => setModalIsOpen(true)} variant="outline-dark"> + </Button>
                    </div>                 
                </Row>  
                <Row className="ml-1" >
                    <Card.Subtitle className="text-muted"> {playlist.playlistDesc} </Card.Subtitle>
                </Row>
            </Card.Header>         
            <Card.Body>
                {playlistTracks.map( track => (
                    <Row key={track.trackName} className="ml-1 mt-2">
                        <img src={track.trackAlbumUrl} alt="" style={{ height: "60px", width: "60px"}}/>
                        <div className="ml-3 right-align">
                            <div >
                                <div className="align-items-center">
                                    {track.trackName}
                                    <XCircle className="ml-2" style={{cursor: "pointer"}} 
                                    onClick={() => 
                                        handleRemoveTrackFromPlaylist(playlist.playlistID, track.trackURI, track.trackPos)}
                                    />
                                </div>
                            </div>                        
                            <div className="text-muted">
                                {track.trackArtist}
                            </div>                                 
                        </div>
                    </Row>
                ))}
            </Card.Body>
            </Card>
        </Container>
    )
}

export default Playlist