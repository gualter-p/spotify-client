import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import { spotifyApi } from '../../auth/SpotifyAPI'
import {Container, Row, Col} from 'react-bootstrap'

const ArtistAlbums = ({artist_id}) => {

    // State for artist albums
    const [albums, setAlbums] = useState([])

    // Get artist's albums
    useEffect(() => {
        // Make the request
        spotifyApi.getArtistAlbums(artist_id).then( res => {   

            // Filter repeated albums 
            const uniqueBy = (a, cond) => {
                return a.filter((e, i) => a.findIndex(e2 => cond(e, e2)) === i);
            }

            // Save the result
            const result = res.body.items.map( album => {
                return {
                    albumName: album.name,
                    albumType: album.type,
                    albumReleaseDate: album.release_date,
                    albumImage: album.images[0].url,
                    albumID: album.id
                }
            })
            const filteredResult = uniqueBy(result, (o1, o2) => o1.albumName === o2.albumName)
            setAlbums(filteredResult)
        })
    }, [artist_id])

    return (         
        <Container>  
            <Row>                   
                {albums.map(album => (
                    <Col key={album.albumID} xs="4">   
                        <Container className="d-flex p-2 flex-column align-items-center">
                            <Link to={`/album/${album.albumID}`}>
                                <img src={album.albumImage} alt="" style={{ height: "150px", width: "150px"}}/>
                            </Link>                        
                            <div>
                                {album.albumName}
                            </div>
                            <div className="text-muted">
                                {album.albumReleaseDate}
                            </div>
                        </Container>                 
                    </Col>
                ))}
            </Row>
        </Container>
    )
}

export default ArtistAlbums