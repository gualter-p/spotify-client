import React from 'react'
import { useParams } from 'react-router-dom'

import ArtistAlbums from './ArtistAlbums'
import ArtistInfo from './ArtistInfo'

const Artist = ({token}) => {

    // Destructure artist_id param
    const {artist_id} = useParams()  

    return(
        <div>
            <ArtistInfo token={token} artist_id={artist_id} />
            <ArtistAlbums token={token} artist_id={artist_id} />
        </div>
    )
}

export default Artist