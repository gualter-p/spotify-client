import React, {useState, useEffect} from 'react'
import { spotifyApi } from '../../auth/SpotifyAPI'
import {Row, Col, Card, Container} from 'react-bootstrap'

const ArtistInfo = ({token, artist_id}) => {

    const [artistInfo, setArtistInfo] = useState([])
    
    // Get artist info
    useEffect(() => {
        // Make the request
        spotifyApi.getArtist(artist_id).then( res => {    
            const result = {
                artistFollowers: res.body.followers.total,
                artistName: res.body.name,
                artistGenres: res.body.genres.pop(),
                artistImage: res.body.images[0].url
            }
            setArtistInfo(result)
        })
    }, [artist_id])

    return (
        <Container className="d-flex p-2 flex-column align-items-center">
            <Card.Title style={{fontSize: "30px"}}>{artistInfo.artistName}</Card.Title>
            <Col className="d-flex p-2 flex-column align-items-center">                   
                <Card.Img className="col- rounded-circle" src={artistInfo.artistImage} alt="" style={{ height: "250px", width: "250px"}}/>
                <Row>
                    <div className="span6 p-2 text-center">
                        <div> {artistInfo.artistFollowers} following </div>
                    </div>                   
                    <div className="span6 p-2 text-center">
                        <div style={{textTransform: "capitalize"}}>
                            {artistInfo.artistGenres}
                        </div>                       
                    </div>    
                </Row>
            </Col>
        </Container>
    )
}

export default ArtistInfo