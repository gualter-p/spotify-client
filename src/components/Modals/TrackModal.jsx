import React from 'react'
import {Row, Container} from 'react-bootstrap'

const TrackModal = ({track, chooseTrack}) => {

    // Handle track choice
    const handleChoice = () => {
        chooseTrack(track)
    }

    return (
        <Container style={{cursor: "pointer"}} onClick={handleChoice} className="d-flex m-2 align-items-center">
            <Row>
                <img src={track.albumUrl} alt="" style={{ height: "65px", width: "65px"}}/>
                <div className="ml-3 right-align">
                    <div>        
                        {track.title}
                    </div>            
                    <div className="text-muted">
                        {track.artist}
                    </div>           
                </div>
            </Row>            
        </Container>
    )
}

export default TrackModal