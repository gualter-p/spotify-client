import React, {useState, useEffect} from 'react'
import { Modal, Form, Button, Row } from 'react-bootstrap'
import TrackModal from '../Modals/TrackModal'
import { spotifyApi } from '../../auth/SpotifyAPI'

const AddTrackToPlaylistModal = ({token, playlist, isOpen, setModalOpen}) => {

    // State for user search input
    const [search, setSearch] = useState("")

    // State for serach results
    const [searchResults, setSearchResults] = useState([])

    // State for selected song
    const [trackToAdd, setTrackToAdd] = useState()

    // Set search
    useEffect(() => {
        if (!search) return setSearchResults([])
        if (!token) return 
        
        // Make the request
        spotifyApi.searchTracks(search).then( res => {
            setSearchResults(res.body.tracks.items.map(track => {
                return {
                    artist: track.artists[0].name,
                    artistID: track.artists[0].id,
                    title: track.name,
                    uri: track.uri,
                    albumUrl: track.album.images[0].url,
                    previewUrl: track.preview_url
                }
            }))
        })
    }, [search, token])

    // Sets track to add
    const chooseTrack = (track) => {
        setTrackToAdd(track)
        setSearch("")
    }

    const handleSubmit = () => {
        spotifyApi.addTracksToPlaylist(playlist.playlistID, [trackToAdd.uri])
            .then( data => {
                setModalOpen(false)
            }, err => {
                console.log('Something went wrong!', err);
            }
        );        
    }

    return (
        <Modal show={isOpen} onHide={() => setModalOpen(false)}>
            <Modal.Header closeButton>
                <Modal.Title> Select the track </Modal.Title>
            </Modal.Header> 
            <Modal.Body>
                <div className="d-flex flex-column py-2">
                    <Form.Control type="search" placeholder="Search Songs/Artists" 
                          value={search} onChange={(e) => setSearch(e.target.value)}>
                    </Form.Control>
                </div>
                <div className="flex-grow-1 my-2" style={{ overflowY: "auto"}}>
                    {searchResults.slice(0,5).map(track => (
                        <TrackModal track={track} key={track.uri} chooseTrack={chooseTrack}/>
                    ))}
                </div>
                {trackToAdd && 
                    <Row className="d-flex mx-auto align-items-center">
                        <img src={trackToAdd.albumUrl} alt="" style={{ height: "60px", width: "60px"}}/>
                        <div className="ml-4">
                            {trackToAdd.title} by {trackToAdd.artist}      
                        </div>             
                    </Row>
                }
                </Modal.Body>   
                <Modal.Footer>
                    <Button variant="primary" onClick={() => handleSubmit()}> Add to playlist </Button>
                    <Button variant="secondary" onClick={() => {setModalOpen(false); setTrackToAdd()}}>
                        Close
                    </Button>  
                </Modal.Footer>       
        </Modal>
    )
}

export default AddTrackToPlaylistModal;