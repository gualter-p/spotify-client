import React from 'react'
import AudioPlayer from 'react-h5-audio-player'
import 'react-h5-audio-player/lib/styles.css';

const Player = ({token, trackUrl}) => {
    
    return (
        <AudioPlayer 
            autoPlay 
            src={trackUrl}
        />     
    )
}

export default Player