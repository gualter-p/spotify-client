import React, {useState, useEffect} from 'react'
import { Container, Form } from 'react-bootstrap'
import { spotifyApi } from '../../auth/SpotifyAPI'

import Track from '../Track/Track'
import Player from '../Player/Player'

const Dashboard = ({token}) => {

    // State for user search input
    const [search, setSearch] = useState("")

    // State for serach results
    const [searchResults, setSearchResults] = useState([])

    // State for selected song
    const [playingTrack, setPlayingTrack] = useState()

    // To choose the playing track 
    const chooseTrack = (track) => {
        setPlayingTrack(track)
    }

    // Set access token
    useEffect(() => {
        if (!token) return
        spotifyApi.setAccessToken(token);
    }, [token]);

    // Set search
    useEffect(() => {
        if (!search) return setSearchResults([])
        if (!token) return 
        setPlayingTrack()
        
        // Make the request
        spotifyApi.searchTracks(search).then( res => {
            setSearchResults(res.body.tracks.items.map(track => {
                return {
                    artist: track.artists[0].name,
                    artistID: track.artists[0].id,
                    title: track.name,
                    uri: track.uri,
                    trackID: track.id,
                    albumUrl: track.album.images[0].url,
                    previewUrl: track.preview_url
                }
            }))
        })
    }, [search, token])

    return (
        <Container>
            {token && <div className="d-flex flex-column py-2">
                <Form.Control type="search" placeholder="Search Songs/Artists" 
                      value={search} onChange={(e) => setSearch(e.target.value)}>
                </Form.Control>
            </div>}
             <div className="flex-grow-1 my-2">
                {searchResults.map(track => (    
                    <Track token={token} track={track} key={track.uri} chooseTrack={chooseTrack}/> 
                ))}
            </div>
           {token && <div className="fixed-bottom">
               <Player token={token} trackUrl={playingTrack?.previewUrl}/>
           </div>}
        </Container>        
    )
}

export default Dashboard;