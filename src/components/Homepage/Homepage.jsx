import React from 'react'

import Dashboard from './Dashboard'

const Homepage = ({token}) => (
    <Dashboard token={token}/>
)

export default Homepage