import React, {useState, useEffect} from 'react'
import { useParams } from 'react-router-dom'
import { spotifyApi } from '../../auth/SpotifyAPI'
import {Container, Col, Row, Card, ListGroup} from 'react-bootstrap'

const Album = () => {

    // Destructure album_id param
    const {album_id} = useParams()

    // State for album info
    const [albumInfo, setAlbumInfo] = useState([])

    // Get album info
    useEffect(() => {
        // Make the request
        spotifyApi.getAlbum(album_id).then( res => {         
            const result = {
                albumName: res.body.name,
                albumArtists: res.body.artists,
                albumTracks: res.body.tracks.items,
                albumID: res.body.id,
                albumImage: res.body.images[0].url,
                albumLabel: res.body.label,
                albumReleaseDate: res.body.release_date
            }
            setAlbumInfo(result)
        })
    }, [album_id])

    return(
        <Container className="d-flex p-2 flex-column align-items-center">
            <Card.Title style={{fontSize: "30px"}}>{albumInfo.albumName}</Card.Title>
            <Col className="d-flex p-2 flex-column align-items-center">                   
                <Card.Img src={albumInfo.albumImage} alt="" style={{ height: "230px", width: "230px"}}/>
                <Row>
                    <div className="span6 p-2 text-center">
                        <Card.Text>Label</Card.Text>{albumInfo.albumLabel}
                    </div>                   
                    <div className="span6 p-2 text-center">
                        Release Date 
                        <div style={{textTransform: "capitalize"}}>
                            {albumInfo.albumReleaseDate}
                        </div>                       
                    </div>   
                </Row>
                <Col>
                    <ListGroup variant="flush">
                        {albumInfo.albumTracks ? albumInfo.albumTracks.map( track => (
                            <ListGroup.Item key={track.id}>{track.name}</ListGroup.Item>
                        )): albumInfo.albumName}
                    </ListGroup>         
                </Col>
            </Col>
        </Container>
    )
}

export default Album