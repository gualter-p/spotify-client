import React from "react";
import {Navbar} from 'react-bootstrap'
import MainNav from "./MainNav";
import AuthNav from "./AuthNav";

const NavBar = (props) => {
  return (
    <div className="nav-container mb-3">
      <Navbar className="navbar navbar-expand-md navbar-light bg-light">
        <div className="container">
          <div className="navbar-brand logo" />
          <MainNav token={props.token}/>
          <AuthNav token={props.token} setToken={props.setToken}/>
        </div>
      </Navbar>
    </div>
  );
};

export default NavBar;