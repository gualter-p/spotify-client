import React from "react";
import AuthenticationButton from "../../auth/components/AuthButton";

import {Navbar} from 'react-bootstrap'

const AuthNav = (props) => (
  <Navbar className="navbar-nav ml-auto">
    <AuthenticationButton token={props.token} setToken={props.setToken}/>
  </Navbar>
);

export default AuthNav;