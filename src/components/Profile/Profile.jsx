import React, {useState, useEffect} from 'react'
import { spotifyApi } from '../../auth/SpotifyAPI'
import profile from '../../assets/profile.png'
import {Container, Row} from 'react-bootstrap'

import NoTokenError from '../Error/NoTokenError'
import UserPlaylists from '../Playlist/UserPlaylists'
import SavedTracks from '../Track/SavedTracks'

const Profile = ({token}) => {

    // State for user info
    const [userInfo, setUserInfo] = useState({})

    // Get user info
    useEffect(() => {
        // Here was the problem.
        if (!token) return 

        // Make the request
        spotifyApi.getMe().then( res => {    
           const result = {
               userName: res.body.display_name,
               userEmail: res.body.email,
               userFollowers: res.body.followers.total,
               userID: res.body.id
           }
           setUserInfo(result)
        })
    }, [token])

    return (
        <div>        
            {!token ? <NoTokenError/> :
            <Container>
                <Container className="d-flex justify-content-md-center align-content-center">
                    <Row className="col-sm-4 d-flex flex-column align-items-center"> 
                        <h1> {userInfo.userName} </h1>
                        <img src={profile} alt="" style={{ height: "150px", width: "150px"}}/>
                        <h6> {userInfo.userFollowers} followers </h6>
                        <p/>
                    </Row>        
                    <Row className="col-sm-8 d-flex flex-column align-items-center mt-2">
                        <h2>Saved tracks</h2>
                        <SavedTracks token={token}/>
                    </Row>
                </Container>
                <UserPlaylists token={token} userid={userInfo.userID} username={userInfo.userName}/>
            </Container>
            }
         </div>
    )
}

export default Profile