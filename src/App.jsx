import React , {useState, useEffect} from 'react'
import { Route, Switch } from "react-router-dom";
import './App.css';

import {hash} from './auth/SpotifyAPI'
import NavBar from './components/Navbar/NavBar'
import Homepage from './components/Homepage/Homepage'
import Profile from './components/Profile/Profile'
import Artist from './components/Artist/Artist'
import Album from './components/Album/Album'

const App = () => {  

  // State for token
  const [token, setToken] = useState("")

  // Extract token
  useEffect(() => {
    let _token = hash.access_token
    if(_token)
      setToken(_token)
  }, []) 

  return (
    <div className="App">
      <NavBar token={token} setToken={setToken}/>
      <div className="container flex-grow-2">
        <Switch>
          <Route exact path="/" render={() => (<Homepage token={token}/>)}/>
          <Route exact path="/profile" render={() => (<Profile token={token}/>)}/>
          <Route exact path="/artist/:artist_id" render={() => (<Artist token={token}/>)}/>
          <Route exact path="/album/:album_id" render={() => (<Album token={token}/>)}/>
        </Switch>
      </div>
    </div>
  );
}

export default App;
