import React from "react";

const LoginButton = () => {

  const authEndpoint = 'https://accounts.spotify.com/authorize';

  const clientId = "2e0e81e8412a465a93b6f8e3652a16b8";
  const redirectUri = "http://localhost:3000/";
  const scopes = [    
    "streaming",
    "user-read-email",
    "user-read-private",
    "user-library-read",
    "user-library-modify",
    "user-read-playback-state",
    "user-modify-playback-state",
    "user-read-currently-playing",
    "playlist-modify-public",
    "playlist-modify-private"
  ];

  return (
    <a href={`${authEndpoint}?client_id=${clientId}&response_type=token&redirect_uri=${encodeURI(redirectUri)}&scope=${scopes.join("%20")}&show_dialog=true`}>
      <button
        className="btn btn-primary btn-block"      
      >
        Log In
      </button>
    </a>
  );
};

export default LoginButton;