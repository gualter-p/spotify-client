import React from "react";

import LoginButton from "./LoginButton";
import LogoutButton from "./LogoutButton";

const AuthenticationButton = (props) => {
  return props.token ? <LogoutButton setToken={props.setToken} /> : <LoginButton />;
};

export default AuthenticationButton;