import React from "react";
import {Link} from 'react-router-dom'

const LogoutButton = (props) => {
   return (   
     <Link to="/">
      <button
        className="btn btn-danger btn-block"   
        onClick={() => props.setToken("")}   
      >
        Log Out
      </button>
      </Link>
  );
};

export default LogoutButton;