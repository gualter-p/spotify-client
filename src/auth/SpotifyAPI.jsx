import SpotifyWebApi from 'spotify-web-api-node'
import env from 'react-dotenv'

export const spotifyApi = new SpotifyWebApi({
    clientId: env.CLIENT_ID
})

// For getting access token
export const hash = window.location.hash
    .substring(1)
    .split("&")
    .reduce(function(initial, item) {
      if (item) {
        var parts = item.split("=");
        initial[parts[0]] = decodeURIComponent(parts[1]);
      }
      return initial;
    }, {});